# This file is part of Controls Showcase by Alexis Laferrière
#
# To the extent possible under law, the person who associated CC0 with
# Controls Showcase has waived all copyright and related or neighboring rights
# to Controls Showcase.
#
# You should have received a copy of the CC0 legal code along with this
# work. If not, see http://creativecommons.org/publicdomain/zero/1.0/.

# Simple app to showcase app.nit controls
module showcase is
	app_name "Controls Showcase"
	app_version(0, 1, git_revision)
	app_namespace "com.paninit.app_showcase"
	android_api_target 22
end

import app::ui
import android::aware

redef class App
	redef fun on_create
	do
		push_window new ShowcaseWindow
		super
	end
end

# Window showing off app.nit controls
class ShowcaseWindow
	super Window

	# Root window layout holding all the controls
	var layout = new VerticalLayout(parent=self)

	# Readable label, probably the simplest control
	var lab3l = new Label(parent=layout, text="Label")

	# Clickable button, usually the most useful control
	var button = new Button(parent=layout, text="Button")

	# Toggleable check box, intrinsically the funniest control
	var check_box = new CheckBox(parent=layout, text="CheckBox")

	# User modifiable text input, consistently the handiest control
	var text_input = new TextInput(parent=layout, text="TextInput")

	# ---
	# Login line: username, password and submit button

	# Layout organizing the 3 controls on a line
	var horizontal = new HorizontalLayout(parent=layout)

	# Login username
	var login_user = new TextInput(parent=horizontal, text="username")

	# Login password, notice the `is_password=true`
	var login_password = new TextInput(parent=horizontal, text="password", is_password=true)

	# Button to submit credentials
	var login_submit = new Button(parent=horizontal, text="Login")

	# ---
	# List of many buttons

	# Scrolling gist holding the button
	var list = new ListLayout(parent=layout)

	# Many buttons
	var list_items: Array[View] =
		[for i in 32.times do new Button(parent=list, text="List item {i}")]

	redef fun on_event(event)
	do
		if event isa ButtonPressEvent and event.sender == button then
			lab3l.text = "The first Button was pressed!"
		else if event isa ToggleEvent and event.sender == check_box then
			lab3l.text = "The CheckBox was toggled to {event.sender.is_checked}"
		else
			lab3l.text = event.to_s
		end
	end
end
