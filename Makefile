bin/showcase: $(shell nitls -M src/showcase.nit -m linux)
	nitc -o $@ src/showcase.nit -m linux

bin/showcase.apk: $(shell nitls -M src/showcase.nit -m android)
	nitc -o $@ src/showcase.nit -m android

bin/showcase.app: $(shell nitls -M src/showcase.nit -m ios)
	nitc -o $@ src/showcase.nit -m ios
